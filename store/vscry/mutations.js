import Vue from 'vue'
const mutations = {
  addCard(state, {name, card}) {
    Vue.set(state.cards, name, card)
  },
  addCatalog(state, {name, catalog}) {
    Vue.set(state.catalogs, name, catalog)
  },
  addQuery(state, {query, results}) {
    Vue.set(state.queries, query, results)
  },
  setTooltip(state, tooltip) {
    Vue.set(state, 'tooltip', tooltip)
  },
  setSearch(state, value) {
    Vue.set(state, 'searching', value)
  }
}

export default mutations
