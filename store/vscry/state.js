const state = () => ({
  cards: {},
  catalogs: {},
  queries: {},
  tooltip: {},
  searching: false
})


export default state
