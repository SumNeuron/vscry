import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'
import state from './state.js'

const namespaced = true




export default {
  namespaced,
  actions,
  getters,
  mutations,
  state
}
