import scryios from 'scryios'

const actions = {
  async fetchCatalog({state, commit}, {catalog}) {
    if (state.catalogs[catalog] !== undefined) return
    let res = await scryios.fetchCatalog(catalog)
    if (res !== undefined) {
      commit('addCatalog', {name:catalog, catalog:res})
    }
    return res
  },

  async delayDispatch({dispatch}, {action, payload}) {
    setTimeout(async function() {
      // console.log('delayed', payload)
      await dispatch(action, payload)
    }, payload.delay)
  },

  async fetchCardByName({state, dispatch, commit}, {name, delay=2000}) {
    let $name = name.toLowerCase().trim()
    if (state.cards[$name] !== undefined) {
      return state.cards[$name]
    }

    if (state.searching === true) {
      await dispatch('delayDispatch', {
        action:'fetchCardByName',
        payload:{name:$name, delay:delay},
      })
      // return
      // setTimeout(async function() {
      //   if (state.cards[$name] === undefined) {
      //     await dispatch('fetchCardByName', {name, delay})
      //     await commit('setSearch', false)
      //   } else {
      //     return state.cards[$name]
      //   }
      // }, delay);
    } else {
      await commit('setSearch', true)
      // console.log($name, state.cards[$name])
      let res
      try {
        await scryios.sleep(scryios.goodCitizenRate)
        res = await scryios.fetchCardByName($name)
      } catch (e) {
        res = undefined
        await commit('setSearch', false)
      }

      if (!(
        typeof res === 'object' &&
        res.object && res.object === 'card')
      ) {
        // console.log('error here')
        await commit('setSearch', false)
        return null
      }

      let cardName = res.name.toLowerCase().trim()
      let faceNames = cardName.split(' // ')
      let faceIdx = 0
      let isMultiface = false
      if (cardName.includes(' // ')) {
        isMultiface = true
        for (let i = 0; i < faceNames.length; i++) {
          let n = faceNames[i].trim()
          if (n === $name) {
            faceIdx = i
          }
        }
      }

      let card = res
      if (isMultiface) {
        card = {...card, ...card.card_faces[faceIdx]}
      }

      await commit('addCard', {name:$name, card})
      await commit('setSearch', false)
      return card
    }
  },
  async fetchCardsByQuery({state, commit}, {query}) {
    if (state.queries[query] !== undefined) {
      return state.queries[query]
    }

    let res = await scryios.searchAll(query)
    let cardNames = []
    if (res !== undefined) {
      if (Array.isArray(res)) {
        res.forEach(card=>{
          let name = card.name.toLowerCase()
          if (name.includes('//')) {
            let faceNames = cardName.split(' // ')
            for (let i = 0; i < faceNames.length; i++) {
              let faceName = faceNames[i]
              cardNames.push(faceName)
              commit('addCard', {
                name:faceName,
                card:{...card, ...card.card_faces[i]}
              })
            }
          } else {
            cardNames.push(name)
            commit('addCard', {name:name, card})
          }
        })
      }
      commit('addQuery', {query, results:cardNames})
    }
    return cardNames
  }
}


export default actions
