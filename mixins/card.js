export default {
  props: {
    name: {
      String,
      default: null
    },
    code: {
      type: String,
      default: null
    },
    number: {
      type: Number,
      default: null
    },
    multiverseId: {
      type: Number,
      default: null
    },
    mtgoId: {
      type: Number,
      default: null
    },
    scryfallId: {
      type: String,
      default: null
    },
    first: {
      type: [Number, String],
      default: null
    },
    second: {
      default: null
    },
    module: {
      type: String
      default: 'vscry'
    }
  },
  computed: {
    card() {
      return this.$store.getters[`${this.module}/card`](this.name)
    },
  }
}
