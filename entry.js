import Vue from 'vue';

import module from './store/vscry/index.js'
import ManaCost from './components/ManaCost.vue'
import Pip from './components/Pip.vue'
import ScryfallSearch from './components/ScryfallSearch.vue'
import CardImage from './components/CardImage.vue'
import CardTooltip from './components/CardTooltip.vue'
import ScryfallQuery from './components/ScryfallQuery.vue'
import Logo from './components/Logo.vue'
import CardLink from './components/CardLink.vue'


const components = {
  ManaCost,
  Pip,
  ScryfallSearch,
  CardImage,
  CardTooltip,
  ScryfallQuery,
  Logo,
  CardLink
}



function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export default components
export const strict = false

export {
  module,
  ManaCost,
  Pip,
  ScryfallSearch,
  CardImage,
  CardTooltip,
  ScryfallQuery,
  Logo,
  CardLink
}
